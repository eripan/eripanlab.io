entries := src/index.pug

.PHONY: all
all: .installed dist

.installed: package.json package-lock.json
	@echo "Dependencies files are newer than .installed; (re)installing."
	npm clean-install
	@echo "This file is used by 'make' for keeping track of last install time. If package.json or package-lock.json are newer than this file (.installed) then all 'make *' commands that depend on '.installed' know they need to run npm clean-install first." \
		> .installed

# Run development server
.PHONY: run
run: .installed
	npx parcel ${entries}

# Build distribution files and place them where they are expected
.PHONY: dist
dist: .installed
	npx parcel build ${entries}

# Nuke from orbit
clean:
	rm -rf dist/ node_modules/
	rm -f .installed
